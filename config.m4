dnl $Id$
dnl config.m4 for extension template

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(template, for template support,
dnl Make sure that the comment is aligned:
dnl [  --with-template             Include template support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(template, whether to enable template support,
dnl Make sure that the comment is aligned:
[  --enable-template           Enable template support])

if test "$PHP_TEMPLATE" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-template -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/template.h"  # you most likely want to change this
  dnl if test -r $PHP_TEMPLATE/$SEARCH_FOR; then # path given as parameter
  dnl   TEMPLATE_DIR=$PHP_TEMPLATE
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for template files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       TEMPLATE_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$TEMPLATE_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the template distribution])
  dnl fi

  dnl # --with-template -> add include path
  dnl PHP_ADD_INCLUDE($TEMPLATE_DIR/include)

  dnl # --with-template -> check for lib and symbol presence
  dnl LIBNAME=template # you may want to change this
  dnl LIBSYMBOL=template # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $TEMPLATE_DIR/lib, TEMPLATE_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_TEMPLATELIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong template lib version or lib not found])
  dnl ],[
  dnl   -L$TEMPLATE_DIR/lib -lm
  dnl ])
  dnl
  dnl PHP_SUBST(TEMPLATE_SHARED_LIBADD)

  PHP_NEW_EXTENSION(template, template.c, $ext_shared)
fi
