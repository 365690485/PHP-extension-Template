/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_TEMPLATE_H
#define PHP_TEMPLATE_H

extern zend_module_entry template_module_entry;
#define phpext_template_ptr &template_module_entry

#define PHP_TEMPLATE_VERSION "0.1.0" /* Replace with version number for your extension */

#ifdef PHP_WIN32
#	define PHP_TEMPLATE_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_TEMPLATE_API __attribute__ ((visibility("default")))
#else
#	define PHP_TEMPLATE_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

extern zend_class_entry *ext_view_ce, *ext_view_impl_ce;

#define FETCH_THIS                    Z_OBJCE_P(getThis()), getThis()
#define EXT_STARTUP_FUNCTION(module)  ZEND_MINIT_FUNCTION(ext_##module)
#define EXT_STARTUP(module)           ZEND_MODULE_STARTUP_N(ext_##module)(INIT_FUNC_ARGS_PASSTHRU)

EXT_STARTUP_FUNCTION(view);

PHP_MINIT_FUNCTION(template);
PHP_MSHUTDOWN_FUNCTION(template);
PHP_RINIT_FUNCTION(template);
PHP_RSHUTDOWN_FUNCTION(template);
PHP_MINFO_FUNCTION(template);

#if ((PHP_MAJOR_VERSION == 5) && (PHP_MINOR_VERSION > 2)) || (PHP_MAJOR_VERSION > 5)
#define STORE_EG_ENVIRON() \
	{ \
		zval ** __old_return_value_pp   = EG(return_value_ptr_ptr); \
		zend_op ** __old_opline_ptr  	= EG(opline_ptr); \
		zend_op_array * __old_op_array  = EG(active_op_array);

#define RESTORE_EG_ENVIRON() \
		EG(return_value_ptr_ptr) = __old_return_value_pp;\
		EG(opline_ptr)			 = __old_opline_ptr; \
		EG(active_op_array)		 = __old_op_array; \
	}

#else

#define STORE_EG_ENVIRON() \
	{ \
		zval ** __old_return_value_pp  		   = EG(return_value_ptr_ptr); \
		zend_op ** __old_opline_ptr 		   = EG(opline_ptr); \
		zend_op_array * __old_op_array 		   = EG(active_op_array); \
		zend_function_state * __old_func_state = EG(function_state_ptr);

#define RESTORE_EG_ENVIRON() \
		EG(return_value_ptr_ptr) = __old_return_value_pp;\
		EG(opline_ptr)			 = __old_opline_ptr; \
		EG(active_op_array)		 = __old_op_array; \
		EG(function_state_ptr)	 = __old_func_state; \
	}

#endif

/* 
  	Declare any global variables you may need between the BEGIN
	and END macros here:     

ZEND_BEGIN_MODULE_GLOBALS(template)
	long  global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(template)
*/

/* In every utility function you add that needs to use variables 
   in php_template_globals, call TSRMLS_FETCH(); after declaring other 
   variables used by that function, or better yet, pass in TSRMLS_CC
   after the last function argument and declare your utility function
   with TSRMLS_DC after the last declared argument.  Always refer to
   the globals in your function as TEMPLATE_G(variable).  You are 
   encouraged to rename these macros something shorter, see
   examples in any other php module directory.
*/

#ifdef ZTS
#define TEMPLATE_G(v) TSRMG(template_globals_id, zend_template_globals *, v)
#else
#define TEMPLATE_G(v) (template_globals.v)
#endif

#endif	/* PHP_TEMPLATE_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
